<?php

namespace test;

class Controller
{
	protected $tpl;
	protected $req;
	
	protected $subscription = 'subs';
	
	protected $app = NULL;
	protected $action = NULL;

	public function __construct()
	{
		$this->tpl = S('Template');
		$this->req = S('Request');
		
		$dirs = $this->req->getDirs();
				
		if(count($dirs) > 1) {
		    $this->app = $this->req->getDir(0);
		    $this->action = $this->req->getDir(1);
		} else {
		    $this->action = $this->req->getDir(0);
		    if(!in_array($this->action, ['subscribe', 'unsubscribe'])) {
		        $this->app = $this->req->getDir(0);
		        $this->action = NULL;
		    }
		}
		
		if( $this->app )
		{
		    $this->subscription = $this->app;
			$this->tpl->setGlob('baseurl', "/{$this->app}");
		}
		else
		{
			$this->tpl->setGlob('baseurl', '');
		}

		session_start();

		$this->_performChecks();
	}

	protected function _subscribe()
	{
	    $_SESSION[$this->subscription] = 1;
	}

	protected function _unsubscribe()
	{
	    $_SESSION[$this->subscription] = 0;
	}

	private function _isSubscribed()
	{
		return isset($_SESSION[$this->subscription]) ? $_SESSION[$this->subscription] : 0;
	}

	private function _performChecks()
	{
		if( !$this->_isSubscribed() && ($this->action != 'subscribe') )
		{
			Response::redirect('/subscribe');
		}
	}
}
